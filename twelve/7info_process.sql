-- 1. Запросы
-- выполняемые запросы
SELECT pid, age(clock_timestamp(), query_start), usename, query, state
FROM pg_stat_activity
WHERE query != '<IDLE>' AND query NOT ILIKE '%pg_stat_activity%'
-- WHERE state not like 'idle%' AND query NOT ILIKE '%pg_stat_activity%'
-- WHERE state != 'idle' AND query NOT ILIKE '%pg_stat_activity%'
ORDER BY query_start desc;

-- 2. Кэш
-- частота попадания в кэш (должна быт не меньше 0,99)
SELECT sum(heap_blks_read) as heap_read,
       sum(heap_blks_hit) as heap_hit,
       (sum(heap_blks_hit) - sum(heap_blks_read)) / sum(heap_blks_hit) as ratio
FROM pg_statio_user_tables;
-- сколько индексов в кэше
SELECT sum(idx_blks_read) as idx_read,
       sum(idx_blks_hit) as idx_hit,
       (sum(idx_blks_hit) - sum(idx_blks_read)) / sum(idx_blks_hit) as ratio
FROM pg_statio_user_indexes;

-- 3. БД
-- все БД и их размер
SELECT * FROM pg_user;
-- все таблицы и их размер, с/без индексов
SELECT datname, pg_size_pretty(pg_database_size(datname))
FROM pg_database
ORDER BY pg_database_size(datname) DESC;

-- 4. Kill
-- kill все запущенные подключений к текущей БД
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE datname = current_database() AND pid <> pg_backend_pid();
-- kill запущенные запросы
SELECT pg_cancel_backend(procpid);
