<?php

class Feel {
    public function feel() {
        echo "I can feel!";
    }
}

interface Listen {
    public function listen();
}

interface Hear {
    public function hear();
}

class PeopleTwo extends Feel implements Listen, Hear {
    public function listen() {
        echo "I can listen!";
    }

    public function hear() {
        echo "I can hear!";
    }

    public function smth() {
        echo "I can do everything!";
    }
}

$people = new PeopleTwo();
$people->feel();
$people->listen();
$people->hear();
$people->smth();