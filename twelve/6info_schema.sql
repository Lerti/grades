-- 1. таблица
-- Информация о таблице Postgres может быть получена из  information_schema.tables/ pg_catalog.pg_tables
-- Postgres table information can be retrieved either from the information_schema.tables view,
-- or from the pg_catalog.pg_tables view. Below are example queries:
SELECT * FROM information_schema.tables;
SELECT * FROM pg_catalog.pg_tables;
-- таблицы - в схеме team
SELECT table_name FROM information_schema.tables
WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
  AND table_schema IN ('team');

--2. схема
-- выбранная пользователем сейчас схема
SELECT current_schema();
-- все схемы в БД
SELECT * FROM information_schema.schemata;
SELECT * FROM pg_catalog.pg_namespace;

-- 3. БД
-- выбранная пользователем сейчас БД
SELECT current_database();
-- все БД для сервера
SELECT * FROM pg_catalog.pg_database;

-- 4. views
-- все представления по всем схемам в базе данных
SELECT * FROM information_schema.views;
SELECT * FROM pg_catalog.pg_views;

-- 5. столбцы таблиц
-- информация о столбах для таблицы users
SELECT * FROM information_schema.columns
WHERE table_name = 'users'
ORDER BY ordinal_position;

-- 6. Индексы
-- всю информацию об индексах в БД
SELECT * FROM pg_catalog.pg_indexes;

-- 7. Функции
-- все функции в БД, у пользовательских функций столбец routine_definition будет содержать тело функции
SELECT * FROM information_schema.routines
WHERE routine_type = 'FUNCTION';

-- 8. Триггеры
-- все триггеры в БД, столбец action_statement содержит тело триггера
SELECT * FROM information_schema.triggers;
