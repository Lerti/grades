<?php

class Speak {
    public function say() {
        echo "I can speak!";
    }
}

trait Walk {
    public function go() {
        echo "I can walking!";
    }
}

trait See {
    public function see() {
        echo "I can see!";
    }
}

class People extends Speak {
    use Walk, See;
    public function smth() {
        echo "I can do everything!";
    }
}

$people = new People();
$people->say();
$people->go();
$people->see();
$people->smth();