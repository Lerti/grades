class Book {
    constructor(content) {
        this.content = content;
    }
    read() {
        console.log(this.content);
    }
}

class Fantastic extends Book {
    constructor(content, namePart) {
        super(content);
        this.name ='New ' + namePart;
    }
    bye() {
        console.log('Go for ' + this.name);
    }
}

let book = new Book("Some info");
book.read();

let newBook = new Fantastic("Some info", "JS");
newBook.read();
newBook.bye();