# Grade 12

## 1. ООП в PHP: Реализовать аналог множественного наследования
#### 
The difference between Traits and multiple inheritance is in the inheritance part. 
A train is not inherited from, but rather included or mixed-in, the becoming part 
of "this class". Traits also provide a more controlled means of resolving conflicts 
the inevitable rise when using multiple inheritance in the few languages that support 
them (C++). Most modern languages are going the approach of a "traits" or "mixin" style 
system as opposed to multiple-inheritance, largely due to the ability to control ambiguities 
if a method is declared in multiple "mixed-in" classes.
Also, one can not "inherit" static member functions in multiple-inheritance.
## 2. PHP: Использовать в разработке Reflection API
#### 
## 3. Yii2: Настроить контроль доступа на основе RBAC.
#### 
## 4. Yii2: Реализовать REST API
####
## 5. Testing: Решить задачу с использованием TDD
#### -
## 6. PostgreSQL: Получать информацию о схеме с помощью SQL-запросов
#### 
## 7. PostgreSQL: Получать информацию о процессах БД с помощью SQL-запросов
#### 
## 8. JS: Использовать в разработке классы
##### 
## 9. ssh+scp: Выполнять команды
#### 
## 10. Настроить web-сервер на отображение страниц по HTTPS с использованием самоподписанного SSL-сертификата
#### -