## Ssh
### файл конфигурации клиента для команды ssh - ~/.ssh/config
    Host test
    HostName test.example.com
    User neo
    Port 2112
для host test - используем имя test.example.com
### вход на сервер по ssh
    ssh neo@255.256.257.258:21
### выполнение команды на сервере по ssh
    ssh neo@255.256.257.258:21 ls
### копирования своего id_rsa.pub ключа на сервер
    ssh-copy-id neo@255.256.257.258:21
    ssh-copy-id -i ~/.ssh/id_rsa.pub neo@255.256.257.258:21
Эта команда экономит время, чтобы не копировать файлы вручную.
Она просто копирует ~/.ssh/id_rsa.pub (или ключ по умолчанию) 
с вашей системы в ~/.ssh/authorized_keys на удалённом сервере.
### Дополнительные настройки ssh на сервере 
    sudo nano /etc/ssh/sshd_config - откроем файл настроек
    PasswordAuthentication no - запрещает вход при помощи пароля,
    по умолчанию Root доступ по ssh разрешен
    PermitRootLogin no --- запрещает вход через пользователя root


## Scp
### scp опции пользователь1@хост1:файл пользователь2@хост2:файл
    scp /lerti/home/file.txt root@google.ru:/google/home/file2.txt
### каталог - С помощью опции -r
    scp -r /home/home/file root@google.ru:/google/home/
    scp -r /home/home/file/* root@google.ru:/google/home/
### c удаленного сервера -  на локальный компьютер
    scp root@google.ru:/google/home/file.txt /lerti/home/
### с сервера - на сервер
    scp root@google.ru:/google/home/file.txt root@yandex.ru:/yandex/home/
