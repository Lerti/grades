<?php

/**
 * Reflection API - отражение или рефлексия (холоним интроспекции, англ. reflection) означает процесс,
 * во время которого программа может отслеживать и модифицировать
 * собственную структуру и поведение во время выполнения.
 */
class Book
{
    private string $content = 'content';

    /**
     * Возвращает текст
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Меняет текст
    */
    private function changeContent(): void
    {
        $this->content = 'new content';
    }
}

// получаем комментарий
$method = new ReflectionMethod('Book', 'getContent');
echo $method->getDocComment();

// для тестирования private function
$book = new Book();
$prMethod = new ReflectionMethod('Book', 'changeContent');
if ($prMethod->isPrivate()) {
    $prMethod->setAccessible(true);
}
$prMethod->invoke($book);
echo $book->getContent();// new content