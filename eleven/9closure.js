// one
function makeFunc() {
    let hello = "World";
    function displayName() {
        alert(hello);
    }
    return displayName;
}

let myFunc = makeFunc();
myFunc();

// two
function makeAdder(x) {
    return function(y) {
        return x + y;
    }
}

let add5 = makeAdder(5);
let add10 = makeAdder(10);

console.log(add5(2));  // 7
console.log(add10(2)); // 12

//three
function increment() {
    var counter = 0;
    return function() {
        return counter++;
    }
}

let counter = increment();

console.log(counter()); // 0
console.log(counter()); // 1
console.log(counter()); // 2