<?php

/**
 * интерфейс компонента
 */
interface InputFormat
{
    public function formatText(string $text): string;
}

/**
 * конкретный компонент
 */
class TextInput implements InputFormat
{
    public function formatText(string $text): string
    {
        return $text;
    }
}

/**
 * базовый класс Декоратора
 */
class TextFormat implements InputFormat
{
    /**
     * @var InputFormat
     */
    protected $inputFormat;

    public function __construct(InputFormat $inputFormat)
    {
        $this->inputFormat = $inputFormat;
    }

    /**
     * Декоратор делегирует всю работу обёрнутому компоненту.
     */
    public function formatText(string $text): string
    {
        return $this->inputFormat->formatText($text);
    }
}

/**
 * конкретный декоратор 1
 */
class StripTagsFilter extends TextFormat
{
    public function formatText(string $text): string
    {
        $text = parent::formatText($text);
        return strip_tags($text);
    }
}

/**
 * конкретный декоратор 2
 */
class PatternFilter extends TextFormat
{
    private $pattern = "|<script.*?>([\s\S]*)?</script>|i";

    public function formatText(string $text): string
    {
        $text = parent::formatText($text);
        return preg_replace($this->pattern, '', $text);
    }
}

/**
 * Фукнкция вывода текста на сайте
 */
function displayText(InputFormat $format, string $text)
{
    echo $format->formatText($text);
}

// использование ...

$comment = "<b>
Hello! Please go to <a href='http://www.example.com'>homepage</a>.
<script src='http://www.example.com/script.js'>
  helloWorld();
</script>
</b>";

/**
 * без ипользования паттера - небезопасное
 */
$input = new TextInput();
displayText($input, $comment);

/**
 * c использованием 1 обертки - почти безопасное
 */
$filteredInput = new StripTagsFilter(new TextInput());
displayText($filteredInput, $comment);

/**
 * c использованием 2 оберток - очень безопасное
 */
$filteredInput = new StripTagsFilter(new TextInput());
$filteredTwoInput = new PatternFilter($filteredInput);
displayText($filteredTwoInput, $comment);