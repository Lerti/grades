-- Создать представление для SQL-запроса
CREATE VIEW now_fantasy AS
SELECT *
FROM book
WHERE kind = 'fantasy'
AND created_at > (
    SELECT start_at
    FROM centure
    WHERE name = 'Cовеременная литература'
    );