-- 8.  PostgreSQL: Написать SQL-запрос с подзапросами

-- 1.  Сделать запрос заказов пользователя. Включить в запрос столбец "ФИО пользователя":
-- а) как столбец,
SELECT o.*,
       (SELECT u.fio FROM users u WHERE u.id = o.user_id) AS user_fio
FROM orders o
-- б) как таблицу.
SELECT o.*,
       u.fio
FROM orders o,
     (SELECT id, fio FROM users) AS u
WHERE u.id = o.user_id;
SELECT o.*,
       u.fio
FROM orders o INNER JOIN
     (SELECT id, fio FROM users) AS u
     ON u.id = o.user_id)


--2.  Сделать запрос пользователей, у которых есть заказы. Использовать подзапрос:
-- а) как условие;
SELECT *
FROM users u
WHERE u.id IN (SELECT o.user_id FROM ordres o)
-- б) в EXISTS
SELECT *
FROM users u
WHERE EXISTS(SELECT 1 FROM ordres o WHERE o.user_id = u.id)


-- 3.  Заполнить временную таблицу одним запросом

-- Начинаем транзакцию
BEGIN;

-- Создаем временную таблицу на основе SQL-запроса
CREATE TEMP TABLE full_users_with_order ON COMMIT DROP
AS
SELECT *
FROM users u
WHERE u.id IN (SELECT o.user_id FROM ordres o)
ORDER BY u.id DESC;

-- Создаем необходимые индексы на временной таблице
CREATE INDEX ON full_users_with_order(id);

-- Анализируем содержимое временной таблицы
ANALYZE full_users_with_order;

-- Временная таблица будет удалена по завершению транзакции из-за ON COMMIT DROP
COMMIT;