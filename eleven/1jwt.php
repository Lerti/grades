<?php

// get token
const SECRET_KEY = "helloWorld";
$header = '{"typ":"JWT","alg":"HS256"}';
$payload = '{"iss":"grades","name":"lerti"}';
$unsignedToken = \sprintf("%s.%s", base64_encode($header), base64_encode($payload));
$signature = hash_hmac('sha256', $unsignedToken, SECRET_KEY);

$tokenEncoded =  \sprintf("%s.%s.%s", base64_encode($header), base64_encode($payload), base64_encode($signature));


// verify token
$tokenDecodedArr = explode('.', $tokenEncoded);

$externalHeader = $tokenDecodedArr[0];
$externalPayload = $tokenDecodedArr[1];
$externalSignature = $tokenDecodedArr[2];

if (base64_decode($externalSignature) ===
    hash_hmac('sha256' , \sprintf("%s.%s", $externalHeader, $externalPayload), SECRET_KEY)) {
    echo 'matched';
} else {
    echo 'don’t mathe';
}