<?php
// 5.  Yii2: Реализовать работу со связанными данными БД с помощью Active Record
// Реализовать оформление заказа пользователя с помощью AR.
// Предусмотреть уменьшение остатков и кошелька пользователя с помощью транзакций Yii2

//TODO::  1. Таблицы
//Структура таблицы 'user' - все пользователи
//CREATE TABLE 'user' (
//  'id' int() UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
//  'fio' varchar(50) NOT NULL 'ФИО',
//  'email' varchar(50) NOT NULL COMMENT 'Почта',
//  'phone' varchar(50) NOT NULL COMMENT 'Телефон',
//  'wallet' int() NOT NULL COMMENT 'Кошелек',
//)

// Структура таблицы 'product' - продукты
//CREATE TABLE 'product' (
//  'id' int() UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
//  'name' varchar(50) NOT NULL 'Название',
//  'amount' varchar(50) NOT NULL COMMENT 'Количество',
//  'cost' varchar(50) NOT NULL COMMENT 'Стоимость',
//)

// Структура таблицы 'order' - заказы
//CREATE TABLE 'order' (
//  'id' int() UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
//  'user_id' varchar() NOT NULL 'Ссылка на покупателя' FOREIGN KEY user.id,
//  'address' varchar(255) NOT NULL 'Адрес доставки',
//  'status' varchar(255) NOT NULL 'Статус',
//  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата и время создания',
//)

// Структура таблицы 'order_item' - строка заказа
//CREATE TABLE 'order' (
//  'id' int() UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
//  'order_id' varchar() NOT NULL 'Ссылка на заказ' FOREIGN KEY order.id,
//  'product_id' int() NOT NULL COMMENT 'Ссылка на продукт' FOREIGN KEY product.id,
//  `quantity` int() UNSIGNED NOT NULL COMMENT 'Количество в заказе',
//)


//TODO:: 2. Классы

//пользователя
class User extends yii\db\ActiveRecord {
    public static function tableName() {
        return 'user';
    }
    public function getOrder() {
        return $this->hasMany(Order::class, ['user_id' => 'id']);
    }

    public function rules()
    {
        return [
            [['fio', 'email', 'phone', 'wallet'], 'required'],
        ];
    }
}

//продукта
class Product extends yii\db\ActiveRecord {
    public static function tableName() {
        return 'product';
    }
    public function getOrders() {
        return $this->hasMany(OrderItem::class, ['product_id' => 'id']);
    }
}

//заказа
class Order extends yii\db\ActiveRecord {
    public static function tableName() {
        return 'order';
    }

    public function getUser() {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getItems() {
        return $this->hasMany(OrderItem::class, ['order_id' => 'id']);
    }
}

//строки заказа
class OrderItem extends yii\db\ActiveRecord {
    public static function tableName() {
        return 'order_item';
    }
    public function getOrder() {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }
    public function getProduct() {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
}

//TODO:: 3. view оформление заказа
?>
<section>
   <div class="container">
       <div class="row">
           <div class="col-sm-9">
               <h1>Оформление заказа</h1>
               <div id="checkout">
                   <?php
                   $form = ActiveForm::begin(
                       ['id' => 'checkout-form']
                   );
                   ?>
                   <?= $form->field($order, 'address')->textarea(['rows' => 2]); ?>
                   <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
                   <?php ActiveForm::end(); ?>
               </div>
           </div>
       </div>
   </div>
</section>

<?php
//TODO:: 4. Контроллер - оформление заказа
use app\models\Order;
use app\models\OrderItem;

class OrderController extends AppController
{
    public $defaultAction = 'checkout';

    public function actionCheckout()
    {
        $order = new Order();
        if ($order->load(Yii::$app->request->post())) {
            // ...и проверяем эти данные
            if ($order->validate()) {
                $userId = Yii::$app->user->getId();
                $orderId = $order->id;
                $order->status = 'N'; // новый
                $cost = 0;
                foreach ($order->getItems() as $one) {
                    $cost += $one->cost;
                }

                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();

                try {
                    $wallet = $db->createCommand('
                        SELECT u.wallet 
                        FROM user u
                        WHERE u.id = ' . $userId)->queryAll();

                    $cost = $db->createCommand('
                        SELECT SUM(p.cost * or.quantity) 
                        FROM product p,
                             order_item or
                        WHERE p.id = or.peoduct_id
                        AND or.order_id =  ' . $orderId . '
                        ')->queryAll();

                    if ($cost > $wallet) {
                        $transaction->rollBack();
                        throw 'Недостаточно средств для заказа.';
                    }

                    foreach ($order->getItems() as $item) {
                        if ($item->getProduct()->amount < $item->quantity) {
                            $transaction->rollBack();
                            throw 'Недостаточно товаров для заказа.';
                        } else {
                            $db->createCommand('
                                UPDATE product 
                                SET amount = amount - ' . $item->quantity . ' 
                                WHERE id = ' . $item->product_id)->execute();
                        }
                    }

                    $db->createCommand('
                        UPDATE user 
                        SET wallet = wallet - ' . $cost . ' 
                        WHERE id = ' . $userId)->execute();

                    $order->save();
                    $transaction->commit();
                } catch(\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('checkout', ['order' => $order]);
    }
}