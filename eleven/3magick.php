<?php
// 3.  ООП в PHP: Использовать в разработке магические методы
//а.  Реализовать установку и чтение приватных строковых переменных, при установке добавлять к каждой префикс 'Private: '

class A {
    private $name;
    private $data;

    function __get($prop) {
        return $this->$prop;
    }

    function __set($prop, $val) {
        $this->$prop = 'Private: ' . $val;
    }
}

$a = new A();
$a->name = 'World';
$a->data = 'Hello';

echo $a->data . ', ' . $a->name . '!';


//б.  Реализовать вызов из своего класса User публичных методов класса Country

class User
{
    protected $id;
    protected $name;
    protected $country;

    public function __get($prop)
    {
        return $this->$prop;
    }

    public function __set($prop, $val)
    {
        $this->$prop = $val;
    }

    function __call($name, $args)
    {
        $country = new Country($args[0]);
        return $country->getName();
    }
}

class Country
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}

$country = new Country('Country');
echo $country->getName();

$user = new User();
echo $user->getName('User');