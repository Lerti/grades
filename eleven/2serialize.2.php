<?php
// 2.  ООП в PHP: Использовать в разработке методы сериализации объектов
// Реализовать хранение между сессиями объекта настроек пользователя в виде класса UserOptions

// UserOptions.php:
class UserOptions
{
    public $id;
    private $login;
    public $model;

    public function __construct($id, $login)
    {
        $this->id = $id;
        $this->login = $login;
    }

    public function addData($model)
    {
        $this->model = $model;
    }
}

// page0.php:
include("UserOptions.php");

$user = new UserOptions(1, 'login');
$user->addData('model');
$data = serialize($user);
// сохраняем $s где-нибудь, откуда page2.php сможет его получить.
file_put_contents('store.txt', $user);

// page1.php:

include("UserOptions.php");

$store = file_get_contents('store.txt');
$data = unserialize($data);

var_dump($data);