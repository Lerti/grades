<?php

//1. Сделать отдельные файлы для логирования ошибок 403, 404, 500, остальных.
// продумать: уровни исключений, количество и размер файлов

return [
    'bootstrap' => ['log'],
    'timeZone' => 'Europe/Moscow',
    'components' => [
        'log' => [
            'targets' => [
                // 403
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/403.log',
                    'maxFileSize' => 1024,
				    'maxLogFiles' => 10,
				    'categories' => ['yii\web\HttpException:403'],
                ],
                // 404
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/404.log',
                    'maxFileSize' => 1024,
				    'maxLogFiles' => 10,
				    'categories' => ['yii\web\HttpException:404'],
                ],
			    // 500
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/500.log',
                    'maxFileSize' => 1024,
				    'maxLogFiles' => 10,
				    'categories' => ['yii\web\HttpException:500'],
                ],
			    // all
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/all.log',
                    'maxFileSize' => 1024,
				    'maxLogFiles' => 10,
				    'categories' => ['yii\web\HttpException:*'],
				    'except' => [
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:500',
                    ],
                ],
            ],
        ],
    ]
];

//2. Реализовать логирование времени выполнение действий с помощью фильтров
/*namespace app\components;

use Yii;
use yii\base\ActionFilter;

class ActionTimeFilter extends ActionFilter
{
    private $_startTime;

    public function beforeAction($action)
    {
        $this->_startTime = microtime(true);
        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        $time = microtime(true) - $this->_startTime;
        Yii::debug("Action '{$action->uniqueId}' spent $time second.");
        return parent::afterAction($action, $result);
    }
}
*/