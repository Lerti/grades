CREATE ROLE all;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA schema TO all;

CREATE ROLE user;
GRANT SELECT ON ALL TABLES IN SCHEMA schema TO user;
GRANT INSERT ON ALL TABLES IN SCHEMA schema TO user;
GRANT UPDATE ON ALL TABLES IN SCHEMA schema TO user;
GRANT DELETE ON ALL TABLES IN SCHEMA schema TO user;

create table users
(
    id int primary key,
    name string not null,
    last_name string not null,
    created_at timestamp default now()
);

create table user_wishlist
(
    id int primary key,
    user_id int,
    product_id int,
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX fkey_user_wishlist_user on user_wishlist(user_id);
CREATE INDEX fkey_user_wishlist_product on user_wishlist(product_id);

create table products
(
    id int primary key,
    name string not null,
    article int not null,
    description string not null
);