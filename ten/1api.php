<?php

//SPA-салона = salon

[
    'class' => UrlRule::class,
    'controller' => 'salon',
    'patterns' => [
        'GET /' => 'index',
        'GET news' => 'news',
        'GET contacts' => 'contacts',
        'GET feedback' => 'feedback',
        'GET services/<id:[\d+]>' => 'one',
        'GET statistics/' => 'statistics',// статистика посещений
        'DELETE <id:[\d+]>' => 'delete',// удалить раздел услуг
        'POST add' => 'create',
        'PUT <id:[\d+]>/update' => 'update',
    ],
],
[
    'class' => UrlRule::class,// отделы
    'controller' => 'service',
    'prefix' => 'services',
    'patterns' => [
        'GET /' => 'index',
		'GET <id:[\d+]>' => 'view',
		'DELETE <id:[\d+]>' => 'delete',
		'POST add' => 'create',
		'PUT <id:[\d+]>/update' => 'update',
    ],
],
[
    'class' => UrlRule::class,// записи
    'controller' => 'record',
    'prefix' => 'records',
    'patterns' => [
        'GET /' => 'index',
        'GET <id:[\d+]>' => 'view',
        'DELETE <id:[\d+]>' => 'delete',
        'POST add' => 'create',
        'PUT <id:[\d+]>/update' => 'update',
    ],
],