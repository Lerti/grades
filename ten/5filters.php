<?php
//1. Настроить доступ с помощью ACF

//use \yii\filters\AccessControl;
public function behaviors()
{
    return [
        'access' => [
            'class' => AccessControl::class,
            'only' => ['index', 'view', 'update', 'delete', 'login', 'logout'],
            'rules' => [
                [// всем
                    'allow' => true,
                    'actions' => ['login', 'index', 'view'],
                    'roles' => ['?'],
                ],
                [// зарегистирированным
                    'allow' => true,
                    'actions' => ['logout'],
                    'roles' => ['@'],
                ],
                [
                    'allow' => true,
                    'actions' => ['update', 'delete'],
                    'roles' => ['admin'],
                ],

            ],
        ],
    ];
}

//2. Поддерживать форматы ответа JSON и XML на русском и английском языках

use yii\filters\ContentNegotiator;
use yii\web\Response;

public function behaviors()
{
    return [
        [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
            'languages' => [
                'en',
                'ru',
            ],
        ],
    ];
}

//3. Организовать кэширование на стороне сервера

use yii\filters\PageCache;

public function behaviors()
{
    return [
        [
            'class' => 'yii\filters\PageCache',
            'only' => ['index', 'view'],
            'duration' => 60,
            'variations' => [
                \Yii::$app->language,
            ],
            //'dependency' => [ - если измениться время обновления новостей кэш будет признан недействительным
                //'class' => 'yii\caching\DbDependency',
            //'sql' => 'SELECT MAX(updated_at) FROM news',
        //],
        ],
    ];
}

//4. Ограничить CRUD HTTP-методами GET и POST

use yii\filters\VerbFilter;

public function behaviors()
{
    return [
        'verbs' => [
            'class' => VerbFilter::class,
            'actions' => [
                'create' => ['post'],
                'view'   => ['get'],
                'update' => ['post'],
                'delete' => ['post'],
            ],
        ],
    ];
}