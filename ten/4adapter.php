<?php

/**
 * с чем может работать код
 */
class Target
{
    public function request(): string
    {
        return "Some code.";
    }
}
/** несовместисый класс */
//SpecRequest.php
class SpecRequest
{
    public function specificRequest(): string
    {
        return ".ynitsed dna tluaf ym si sihT ";
    }
}

/** делает несовместимый класс совместимым */
//Adapter.php
class Adapter extends Target
{
    private $adaptee;

    public function __construct(SpecRequest $adaptee)
    {
        $this->adaptee = $adaptee;
    }

    public function request(): string
    {
        return "Adapter: " . strrev($this->adaptee->specificRequest());// переворачиваем строку
    }
}

$target = new Target();
echo $target->request();// понятно

$cpecRequest = new SpecRequest();
echo $cpecRequest->specificRequest();// не понятно

$adapter = new Adapter($cpecRequest);
echo $adapter->request();// понятно