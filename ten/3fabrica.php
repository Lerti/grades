<?php

/** абстрактная фабрика */
//AbstractFactory.php
interface AbstractFactory
{
    public function createProductA(): AbstractProductA;

    public function createProductB(): AbstractProductB;
}

/** конкретная фабрика 1 */
//FactoryOne.php
class FactoryOne implements AbstractFactory
{
    public function createProductA(): AbstractProductA
    {
        return new OneProductA();
    }

    public function createProductB(): AbstractProductB
    {
        return new OneProductB();
    }
}

/** конкретная фабрика 2 */
//FactoryTwo.php
class FactoryTwo implements AbstractFactory
{
    public function createProductA(): AbstractProductA
    {
        return new TwoProductA();
    }

    public function createProductB(): AbstractProductB
    {
        return new TwoProductB();
    }
}

/** абстрактный продукт 1 */
//AbstractProductA.php
interface AbstractProductA
{
    public function createA(): string;
}

/** конкретный абстрактный продукт 1 из 1 фабрики*/
//OneProductA.php
class OneProductA implements AbstractProductA
{
    public function createA(): string
    {
        return "This is a product A in1 view.";
    }
}

/** конкретный абстрактный продукт 1 из 2 фабрики*/
//TwoProductA.php
class TwoProductA implements AbstractProductA
{
    public function createA(): string
    {
        return "This is a product A in 2 view.";
    }
}

/** абстрактный продукт2 */
//AbstractProductB.php
interface AbstractProductB
{
    public function createB(): string;

    public function createAandB(AbstractProductA $productA): string;
}

/** конкретный абстрактный продукт 2 из 1 фабрики*/
//OneProductB.php
class OneProductB implements AbstractProductB
{
    public function createB(): string
    {
        return "This is a product B in 1 view.";
    }

    public function createAandB(AbstractProductA $productA): string
    {
        $resultA = $productA->createA();
        $resultB = $this->createB();
        return $resultA . $resultB . " in 1 view.";
    }
}

/** конкретный абстрактный продукт 2 из 2 фабрики*/
//TwoProductB.php
class TwoProductB implements AbstractProductB
{
    public function createB(): string
    {
        return "This is a product B in 2 view.";
    }

    public function createAandB(AbstractProductA $productA): string
    {
        $resultA = $productA->createA();
        $resultB = $this->createB();
        return $resultA . $resultB . " in 2 view.";
    }
}


function client(AbstractFactory $factory)
{
    $productA = $factory->createProductA();
    $productB = $factory->createProductB();

    echo $productB->createB();
    echo $productB->createAandB($productA, $productB);
}