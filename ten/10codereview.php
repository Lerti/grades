<?php

// old
class CheckTypes {
    function getStringSizes($width = NULL, $height = NULL, $depth = NULL, $unit = NULL)
    {
        if (!$width && !$height && !$depth)
            return NULL;
        $strResult = '';
        if ($width)
            $strResult .= $width;
        if ($height) {
            if (!empty($strResult))
                $strResult .= ' x ';
            $strResult .= $height;
        }
        if ($depth) {
            if (!empty($strResult))
                $strResult .= ' x ';
            $strResult .= $depth;
        }
        if ($unit)
            $strResult .= ' ' . $unit;
        return $strResult;
    }

    function InitBVar(&$var)
    {
        $var = ($var=="Y") ? "Y" : "N";
    }


    function is_otricat($val){
        $num = substr( $val, 0, 1 );
        if ( $num == "-" ) {
            return 'число отрицательное';
        } elseif ( $num == 0 ) {
            return 'число равно нулю';
        } else {
            return 'число положительное';
        }
    }

}

//new

class CheckTypesTwo {
    private string $_b;

    private CONST _YES_VALUE = 'Y';
    private CONST _NOT_VALUE = 'N';

    function getStringSizes(int $width = null, int $height = null, int $depth = null, string $unit = null): string
    {
        $strResult = null;
        $strResult .= $width ?: null;
        if ($height) {
            $strResult .= ($strResult ? ' x ' : '') . $height;
        }
        if ($depth) {
            $strResult .= ($strResult ? ' x ' : '') . $depth;
        }

        $strResult .= $unit ? ' ' . $unit : ($strResult ? '' : null);
        return $strResult;
    }

    function setB(string $value): void
    {
        $this->_b = ($value === self::_YES_VALUE) ? $value : self::_NOT_VALUE;
    }

    public function getNumberSign(int $number): string// передавать число -- если строка, перед передачей приводить к (int)
    {
        return 'число ' . (($number >= 0) ? ($number === 0 ? 'равно нулю' : 'положительное') : 'отрицательное');
    }

}