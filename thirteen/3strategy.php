<?php

/**
 * Интерфейс для клиентов.
 */
class Context
{
    /**
     * @var Strategy ссылка на один из объектов Стратегии.
     */
    private $strategy;

    /**
     * Обычно Контекст принимает стратегию через конструктор
     */
    public function __construct(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Обычно Контекст позволяет заменить объект Стратегии во время выполнения.
     */
    public function setStrategy(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Вместо того, чтобы самостоятельно реализовывать множественные версии
     * алгоритма, Контекст делегирует некоторую работу объекту Стратегии.
     */
    public function doSomeBusinessLogic(): void
    {
        echo "Context: smth doing\n";
        $result = $this->strategy->doAlgorithm([1, 2, 3]);
        echo $result . "\n";
    }
}

/**
 * Интерфейс Стратегии объявляет операции, общие
 */
interface Strategy
{
    public function doAlgorithm(array $data): int;
}

/**
 * Конкретные Стратегии реализуют алгоритм, следуя базовому интерфейсу
 * Стратегии. Этот интерфейс делает их взаимозаменяемыми в Контексте.
 */
class ConcreteStrategyA implements Strategy
{
    public function doAlgorithm(array $data): int
    {
        return array_sum($data);
    }
}

class ConcreteStrategyB implements Strategy
{
    public function doAlgorithm(array $data): int
    {
        return array_product($data);
    }
}

/**
 * Клиентский код выбирает конкретную стратегию и передаёт её в контекст. Клиент
 * должен знать о различиях между стратегиями, чтобы сделать правильный выбор.
 */
$context = new Context(new ConcreteStrategyA());
echo "Client: Strategy return sum.\n";
$context->doSomeBusinessLogic();

echo "\n";

echo "Client: Strategy return composition\n";
$context->setStrategy(new ConcreteStrategyB());
$context->doSomeBusinessLogic();