// интерфейс JS для работы с запросами и ответами HTTP

//1. Promise, возвращаемый вызовом fetch() не перейдёт в состояние "отклонено" из-за ответа HTTP,
// который считается ошибкой, даже если ответ HTTP 404 или 500. Вместо этого, он будет выполнен нормально
// (с значением false в статусе ok ) и будет отклонён только при сбое сети или если что-то помешало запросу выполниться.
// 2. По умолчанию, fetch не будет отправлять или получать cookie файлы с сервера, в результате чего запросы
// будут осуществляться без проверки подлинности (можно включить).

const url = 'https://jsonplaceholder.typicode.com/todos';

async function getResponse() {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    const data = await response.json();
    console.log(data);
}

async function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

async function afterOne() {
    await timeout(1000);
   // await new Promise((resolve, reject) => setTimeout(resolve, 1000));
    console.log('after 1');
}

async function afterThree() {
    await timeout(3000);
    console.log('after 3');
}

async function now() {
    console.log('now!');
}

getResponse();
afterOne();
afterThree();
now();
getResponse();