# Yii2 Примеры реализации паттернов проектирования

## 1. Одиночка
### basic\vendor\yiisoft\yii2\base\Module.php
    public static function getInstance()
    {
        $class = get_called_class();
        return isset(Yii::$app->loadedModules[$class]) ? Yii::$app->loadedModules[$class] : null;
    }
    public static function setInstance($instance)
    {
        if ($instance === null) {
            unset(Yii::$app->loadedModules[get_called_class()]);
        } else {
            Yii::$app->loadedModules[get_class($instance)] = $instance;
        }
    }
####
## 2. Builder
порождающий паттерн проектирования, который позволяет создавать сложные объекты пошагово. 
Строитель даёт возможность использовать один и тот же код строительства для получения разных 
представлений объектов.
### basic\vendor\yiisoft\yii2\db\ActiveQueryInterface.php - сборка
basic\vendor\yiisoft\yii2\db\ActiveQuery.php  - 1 представление....
####
## 3. Итератор
поведенческий паттерн проектирования, который даёт возможность последовательно обходить элементы составных объектов, 
не раскрывая их внутреннего представления.
### basic\vendor\yiisoft\yii2\web\SessionIterator.php - отдельно вынесенный итератор; 
basic\vendor\yiisoft\yii2\web\Session.php - где используется
    public function getIterator()
    {
        $this->open();
        return new SessionIterator();
    }
####
## 4. Декоратор 
структурный паттерн проектирования, который позволяет динамически добавлять объектам новую 
функциональность, оборачивая их в полезные «обёртки».
### basic\vendor\yiisoft\yii2\base\ActionFilter.php - отдельно вынесенный объект;
basic\vendor\yiisoft\yii2\filters\AjaxFilter.php и basic\vendor\yiisoft\yii2\filters\AccessControl - добавляеют новую функциональность
####
## 5. Легковес 
структурный паттерн, который экономит память, благодаря разделению общего состояния, 
вынесенного в один объект, между множеством объектов. Позволяет экономить память, 
кешируя одинаковые данные, используемые в разных объектах.
### yii\behaviors\CacheableWidgetBehavior.php
    public function beforeRun($event)
    {
        $cacheKey = $this->getCacheKey();
        $fragmentCacheConfiguration = $this->getFragmentCacheConfiguration();

        if (!$this->owner->view->beginCache($cacheKey, $fragmentCacheConfiguration)) {
            $event->isValid = false;
        }
    }

    /**
     * Outputs widget contents and ends fragment caching.
     *
     * @param WidgetEvent $event `Widget::EVENT_AFTER_RUN` event.
     */
    public function afterRun($event)
    {
        echo $event->result;
        $event->result = null;

        $this->owner->view->endCache();
    }
####
## 5.5 Наблюдатель
### из symphony - symphony/event-dispatcher/ - с натяжкой
The EventDispatcher component provides tools that allow your application
components to communicate with each other by dispatching events and listening to
them.
####

Abstract Factory, Factory Method, Adapter, Composite, Decorator, Facade, Proxy, Flyweight
Chain of responsibility, Command, Observer, State, Strategy, Template method
Dependency Injection, Repository, mixin etc.