# Микросерверная архитектура для сети кабинок 0-транспортировки
0-транспортировка (телепортация)
Через двадцать лет будет создана Нуль-транспортировка. 
Полностью исчезнет транспорт и везде будут стоять будочки. 
Заходишь в будочку на Малом проспекте Василеостровской стороны,
набираешь код "Москва, Пушкинская площадь". 
Тебя быстренько "разбирают на молекулы" сканер записывает формулу твоего текущего состояния 
и передает через Сеть формулу. 
В будке-приемнике на Пушкинской площади, если никого нет, автоматически запирается дверь и тебя "собирают" 
(вместе с твоими пожитками). 
Пара минут - и ты преспокойненько выходишь на Тверскую. Ни боли, ни страха. Время - 1 секунда.

## 1 Выявление микросервисов

### 1.1 Пользовательские истории
    кабинка - управляющий сервис
    разбор одушевленного объекта
    разбор неодушевленного объекта
    определение точки назначения
    перенос одушевленного объекта
    перенос неодушевленного объекта
    сбор неодушевленного объекта
    сбор неодушевленного объекта

### 1.2 Сервисы
    Box - устанавливаем коробку + статистика
    Way - определяем путь по головому вводу
    Animate - разбирает на- и собирает из- молекул одушевленный объект
    Inanimate - разбирает на- и собирает из- молекул неодушевленный объект
    Cat - осуществляет перенос объекта через  варп-пространство

## 2. Бизнес-процессы
![img_1_0.png](img/img_1_0.png)

1. Объект приходит в Box.
2. Вводит в Box путь.
3. Box передает путь в Way - в котором соедержится карта маршрутоа - и получает координаты.
4. Box определяет вид объектов - на не- и одушевленные и передеает в Animate и Inanimate соответственно.
5. В Animate и Inanimate происходит разбор объектов на молекулы, etc. и передача обратно в Box.
6. Box передает данные из Animate и Inanimate + координаты в Cat.
7. Cat осуществляет перенос молекул, etc. - и возвращает результат в Box (разделенный на не- и одушевленный).
8. Box передает результат в Animate и Inanimate соответственно.
9. В Animate и Inanimate происходит сбор объектов из молекул, etc. и передача обратно в Box.
10. В Box происходит объединение обектов - и выдача.

! Если Box упадет, всему конец. В Box собирается статистика. Если упадет Animate, он может замениться Inanimate 
(и наоборот) - интересно, что получим в итоге.

## 3. Логика работы
![img_1_1.png](img/img_1_1.png)

## 4. API - какие

### Box
0. POST объект в Box
1. GET координаты из Way
2. POST молекулы в Animate
3. Animate etc. в Inanimate
4. PATCH объект из Cat (расположение)
5. GET объект из Animate
6. GET объект etc. из Inanimate 
7. GET объект из Box
8. Получение статистики из Box

## 4. API - реализация

### 0. Общее

1. POST


    Успех:
    HTTP 201 Created

2. Ошибка:


    HTTP 500
    Content-Type: application/json
    {
        "errors": [
            {
                "status": "500",
                "detail": "Some detail"
            }
        ]
    }

### 1. Box
1.1 Создать объект

    POST /object
    {
        "data": {
            "type": "human",
            "attributes": {
                "name": "SCHRODINGER's cat",
                // other attributes
            }
        }
    }
    
1.2 Вернуть объект

    GET /object?filter[name]="SCHRODINGER's cat"

    {
        "data": {
            "type": "object",
            "attributes": {
                "name": "SCHRODINGER's cat",
                // other attributes
            }
        }
    }

1.3 Получить статистику (с фильтрами)

    GET /statistics?filter[name]="SCHRODINGER's cat"&filter[date]=today

    {
        "data": {
            "type": "statistics",
            "attributes": {
                "created": "today",
                "from": "one point",
                "to": "another point"
                // other attributes
            }
        }
    }
    ! {objectId} хранится в Box

### 2. Way
2.1 Получить координаты

    GET /way?filter[name]="Соседний дом"

    {
        "data": {
            "type": "coordinates",
            "attributes": {
                "latitude": 59,
                "longitude": 30
            }
        }
    }

### 3. Animate
1.1 Создать молекулы

    POST /molecules/{objectId}
    {
        "data": {
            "type": "molecules",
            "attributes": {
                "name": "SCHRODINGER's cat",
                // other attributes
            }
        }
    }

1.2 Вернуть объект

    GET /object/{objectId}

    {
        "data": {
            "type": "object",
            "attributes": {
                "name": "SCHRODINGER's cat",
                // other attributes
            }
        }
    }

### 4. Inanimate
1.1 Создать etc.

    POST /etc/{objectId}
    {
        "data": {
            "type": "etc",
            "attributes": {
                "name": "SCHRODINGER's cat",
                // other attributes
            }
        }
    }

1.2 Вернуть объект

    GET /object/{objectId}

    {
        "data": {
            "type": "object",
            "attributes": {
                "name": "SCHRODINGER's cat",
                // other attributes
            }
        }
    }

### 4. Cat
1.1 Переместить объект

    PATCH /object/{objectId}
    {
        "data": {
            "type": "object",
            "id": 1,
            "attributes": {
                "latitude": 59,
                "longitude": 30
            }
        }
    }
