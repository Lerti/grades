<?php
/**
 * Строкой задано математическое выражение.
 * Могут быть вещественные положительные числа с 0,
 * знаки действий + - / * скобки () неограниченной вложенности
 * и переменные, задаваемые английскими буквами. Унарного минуса нет. Степени нет, но можно и её)
 * 1) сделать проверку синтаксиса (в том числе - скобок)
 * 2) вычислить результат (переменные задаём заранее или вводим, пофиг;
 * приоритеты операций - как в математике)
 */

// задаем заранее нужные переменные
$x = 5;// можно задать их в классе и получить значение при помощи get_class_vars(get_class(new GetValue()))), но как-то просто
$y = 10;

class GetValue
{
    const PLUS = "+";
    const MINUS = '-';
    const MULTIPLY = '*';
    const DIVISION = "/";
    const OPENING = "(";
    const CLOSING = ")";

    /**
     * Парсим файл в поисках переменных ... конечно, можно было вынести их в отдельных файл...
     */
    private static function getVariables(): array
    {
        $vars = [];
        $content = file(__FILE__ );
        array_walk($content, function ($row) use (&$vars) {
            if (strripos($row, ';') !== false) {
                $clearRow = str_replace(' ', '', $row);
                $rowArray = explode(';', $clearRow);
                array_walk($rowArray, function ($value) use (&$vars) {
                    preg_match_all(
                        '/^\$([a-zA-Z])\=(\d+)$/',
                        $value,
                        $all);

                    if (count($all) === 3 && $all[0][0] !== null) {
                        $vars[$all[1][0]] = (int)$all[2][0];
                    }
                });
            }
        });
        return $vars;
    }

    /**
     * Проверяем значение на число
     */
    private static function checkForD(string $val) {
        return is_numeric($val);
    }

    /**
     * Выполняем операцию
     */
    private static function getNextValue($operation, $all, $i, $answer): array
    {
        $final = [];
        $error = '';
        if (!key_exists($i + 1, $all)) {
            $error = 'Выражение не может оканчиваться на знак.';
        }
        $next = $all[$i + 1];
        if (!self::checkForD($next)) {// проверяем, что после "знака" идет цифра
            $error .= 'После знака должна быть цифра';
        } else {
            $finalAnswer = 0;
            switch ($operation) {
                case(self::PLUS):
                    $finalAnswer = $answer + $next;
                    break;
                case(self::MINUS):
                    $finalAnswer = $answer - $next;
                    break;
                case(self::MULTIPLY):
                    $finalAnswer = $answer * $next;
                    break;
                case(self::DIVISION):
                    $finalAnswer = $answer / $next;
                    break;
            }
            $final['answer'] = $finalAnswer;
            $final['i'] = $i + 1;
        }

        if ($error !== '') {
            $final['error'] = $error;
        }

        return $final;
    }

    /**
     * Выполняем вычисления
     */
    private static function calculate(array $all, int $answer): string
    {
        foreach ($all as $one => $two) {
            if ($two == null) {
                unset($all[$one]);
            }
        }
        $all = array_values($all);
        $someArray = $all;
        //сначала ищем все знаки "*"/ "/" и выполняем с ними действия
        $consts = [self::MULTIPLY, self::DIVISION];
        array_walk($consts, function($const) use (&$all, &$someArray) {
            for ($i = 0; $i < count($all); $i++) {
                if ($all[$i] === $const) {
                    $nextData = self::getNextValue($const, $all, $i, $all[$i - 1]);
                    if (key_exists('error', $nextData)) {
                        return $nextData['error'];
                    }
                    $all[$i + 1] = $nextData['answer'];
                    $someArray[$i + 1] = $nextData['answer'];
                    unset($someArray[$i - 1]);
                    unset($someArray[$i]);
                    $i = $nextData['i'];
                }
            }
            // делаем массив нормальным
            $all = array_values($someArray);
        });


        for ($i = 0; $i < count($all); $i++) {
            if ($i === 0) {
                if (self::checkForD($all[$i])) {// знак "+" игнорируем в начале
                    $answer = $all[$i];
                } else if ($all[$i] === self::MINUS) {
                    $nextData = self::getNextValue(self::MINUS, $all, $i, $answer);
                    if (key_exists('error', $nextData)) {
                        return $nextData['error'];
                    }
                    $answer = $nextData['answer'];
                    $i = $nextData['i'];
                }
            } else if ($all[$i] === self::PLUS) {
                $nextData = self::getNextValue(self::PLUS, $all, $i, $answer);
                if (key_exists('error', $nextData)) {
                    return $nextData['error'];
                }
                $answer = $nextData['answer'];
                $i = $nextData['i'];
            } else if ($all[$i] === self::MINUS) {
                $nextData = self::getNextValue(self::MINUS, $all, $i, $answer);
                if (key_exists('error', $nextData)) {
                    return $nextData['error'];
                }
                $answer = $nextData['answer'];
                $i = $nextData['i'];
            }
        }

        return $answer;
    }

    public static function getResult(string $expression): string
    {
        // убираем пробелы
        $expression = str_replace(' ', '', $expression);

        // проверка синтаксиса
        $allPattern = '/[a-zA-Z*\/+\-()]|\d+/';
        preg_match_all(
            $allPattern,
            $expression,
            $all
        );

        // вычисляем правильную длину выражения - потому что могут быть двухзначные числа
        $newLength = 0;
        foreach ($all[0] as $one) {
            $newLength+= strlen($one);
        }
        if (count($all[0]) == 0 || $newLength !== strlen($expression)) {
            return 'Введенное выражение содержит не разрешенные символы или слишком короткое. ' .
                'Помните, что можно ввести только: числа, a-z, пробел и знаки "/", "*", "-", "+", ")", "(".' .
                'Пожалуйста, учтите эти правила и попробуйте ещё раз.';
        }
        $all = $all[0];
        if (!preg_match('/[+\-(\da-zA-Z]/', $all[0])) {
            return 'Неправильное начало выражения';
        }

        $allVariables = self::getVariables();
        // подставляем значения переменных
        foreach ($all as $key => $value) {
            // проходим по всему массиву ---
            if (preg_match('/[a-zA-Z]/', $value)) {
                if (key_exists($value, $allVariables)) {
                    $all[$key] = $allVariables[$value];
                } else  {
                    return 'Пожалуйста, укажите значение переменной ' . $value;
                }
            }
        }
        // попытка вычисления
        $answer = 0;

        //нолевым действием ищем со скобками части

        $openPosition = 0;
        $open = 0;
        $openArray = [];
        for ($i = 0; $i < count($all); $i++) {
            // проходим по всему массиву ---
            if ($all[$i] === self::OPENING) {
                $openPosition = $i;
                $openArray[] = $openPosition;
                $open++;
            } else if ($all[$i] === self::CLOSING) {
                if (in_array($openPosition, $openArray)) {
                    $key = array_search($openPosition, $openArray);

                } else {
                    $key = max(array_keys($openArray));
                    $openPosition = $openArray[$key];
                }
                unset($openArray[$key]);
                $open--;

                $openCloseArray = array_slice($all, $openPosition + 1, $i- $openPosition - 1);

                $new = self::calculate($openCloseArray, $answer);

                if (!is_numeric($new)) {
                    return $new;
                }

                for ($k = $openPosition; $k < $i; $k++) {
                    $all[$k] = '';
                }
                $all[$k] = $new;
            }
        }

        if ($open != 0) {
            return 'Неправильное количество скобок в выражении';
        }
        $answer = self::calculate(array_values($all), $answer);
        return $answer;
    }
}

$expression = 'y / 5 + (6 - x) * 2 - 1';
echo GetValue::getResult($expression);