<?php

namespace vendor\v1\kernel;

abstract class BaseModel
{
    abstract function getData(string $data): string;
}