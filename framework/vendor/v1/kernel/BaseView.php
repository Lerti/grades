<?php

namespace vendor\v1\kernel;

class BaseView
{
    function generate(string $contentView = '', $data = null): void
    {
        if (is_array($data)) {
            extract($data);
        }
        if ($contentView) {
            $content = $this->template($contentView);
        }

        include 'vendor/v1/views/layout.php';
    }

    protected function template($fileName, $vars = []): string
    {
        foreach ($vars as $k => $v)
        {
            $$k = $v;
        }
        ob_start();
        include ROOT . "$fileName";
        return ob_get_clean();
    }
}