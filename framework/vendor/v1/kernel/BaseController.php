<?php

namespace vendor\v1\kernel;

abstract class BaseController
{
    private const DEFAULT_TITLE = 'Hello';

    protected $view;

    function __construct()
    {
        $this->view = new \vendor\v1\kernel\BaseView();
    }

    protected function render(string $view = '', string $content = '', string $title = ''): void
    {
        $data = [
            'title' => $title ?? self::DEFAULT_TITLE,
            'content' => $content,
            'date' => date("d.m.Y")
        ];
        $this->view->generate($view ? 'Views/' . $view : '', $data);
    }
}