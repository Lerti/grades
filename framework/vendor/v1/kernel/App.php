<?php

namespace vendor\v1\kernel;

class App
{
    private \vendor\v1\components\Router $_router;

    function __constructor() {}

    public function run() {
        $this->_router = new \vendor\v1\components\Router();
        $this->_router->run();
    }
}