<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title><?= $title ?></title>
    <meta content="text/html; charset=UTF-81" http-equiv="content-type">
    <link href="https://fonts.googleapis.com/css?family=Caveat:400,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="../../vendor/v1/views/css/style.css" />
</head>
<body>
<div id="header">
    <div class = "headerText">
        <h1>Интернет-магазин</h1>
    </div>
</div>

<div id="content">
    <?= $content ?>
</div>

<div id="footer">
    &#169; Все права защищены. Адрес. Телефон. <?= $date . " г." ?>
</div>

</body>
</html>