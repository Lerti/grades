<?php

namespace vendor\v1\components;

class Router
{
    private $_routes;

    public function __construct()
    {
        $this->_routes = include(ROOT . '/config/routes.php');
    }

    private function _getUrl(): ?string
    {
        $baseUrl = trim($_SERVER['REQUEST_URI']);
        $route = null;

        if ($baseUrl === '/') {
            return 'main/main';
        }

        foreach($this->_routes as $pattern => $path) {
            if (preg_match("~$pattern~", $baseUrl)) {
                $route = preg_replace("~$pattern~", $path, $baseUrl);
                break;
            }
        }

        return $route;
    }

    public function run(): void
    {
        $url = $this->_getUrl();

        $params = explode('/', $url);
        $controller = "\\Controllers\\" . ucfirst(array_shift($params)) . 'Controller';
        $action = 'action' . ucfirst(array_shift($params));

        $this->_checkError($url, $controller, $action);
        call_user_func_array([(new $controller()), $action], $params);
    }

    private function _checkError($url, $controller, $action): void
    {
        // потому что если нету одного из - с вероятностью 99.999 можно утверждать, что нет и последующих
        $error = null;
        if (!$url) {
            $error = 'Пропишите адрес в routes!';
        } else if (!class_exists($controller)) {
            $error = 'Создайте класс контроллера!';
        } else if (!method_exists(new $controller(),$action)) {
            $error = 'Создайте action!';
        }

        if ($error) {
            die($error);
        }
    }
}