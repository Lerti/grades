<?php

class Autoloader
{
    function __construct()
    {
        spl_autoload_register([$this, 'autoload']);
    }

    protected function autoload($class): bool
    {
        $array = explode('\\', $class);
        $name = array_pop($array);
        $folder = implode('\\', $array);
        $file = "$folder\\$name.php";
        $file = ROOT . str_replace('\\', '/', $file);

        if (file_exists($file)) {
            include $file;
            return true;
        }

        return false;
    }
}