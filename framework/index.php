<?php

// подключение файлов
define('ROOT', dirname(__FILE__) . '/');

// авто-загрузчик
require_once ROOT . '/vendor/v1/components/Autoloader.php';
$autoloader = new Autoloader();

// общие настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

// вызов Router
$app = new \vendor\v1\kernel\App();
$app->run();