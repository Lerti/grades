<?php

return [
    '/hello/([a-z]*)' => 'main/index/$1',
    '/bye/([a-z]*)' => 'main/bye/$1',
    '/error' => 'main/error'
];