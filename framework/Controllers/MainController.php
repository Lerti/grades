<?php

namespace Controllers;

class MainController extends \vendor\v1\kernel\BaseController
{
    private $_model;

    function __construct()
    {
        $this->_model = new \Models\User();
        parent::__construct();
    }

    public function actionMain(): void
    {
        $this->render('main/main.html');
    }

    public function actionIndex(string $name = ''): void
    {
        $title = "Hello $name";
        $data = $this->_model->getHello($name);

        $this->render('', $data, $title);
    }

    public function actionBye(string $name = ''): void
    {
        $title = "Bye $name";
        $data = $this->_model->getHello($name, false);

        $this->render('', $data, $title);
    }

    public function actionError(): void
    {
        $this->render('main/404.html');
    }
}