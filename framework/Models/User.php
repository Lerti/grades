<?php

namespace Models;

class User extends \vendor\v1\kernel\BaseModel
{
    public function getHello(string $name, bool $new = true): string
    {
        return $this->getData(($new ? 'Hello' : 'Goodbye') . " $name!");
    }

    function getData(string $data): string
    {
        // потому что надо что-то сделать, потом будет подключение к БД и тп
        return trim($data);
    }
}